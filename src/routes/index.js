import Express from 'express'
import {connection} from './../config/mysql'

const route = Express.Router()
// Métodos HTTP
// GET => Busca informação
// POST => Salvar uma informação
// PUT => Atualizar uma informação
// DELETE => Deletar uma informação
route.get('/', (req, res)=>{
    res.send('<h2>Voce está na página inicial da api</h2>')
})
route.get('/hello',  (req, res)=>{
    res.send('world')
})
route.get('/perguntas', (_,res)=>{
    connection.query('select * from Pergunta', function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    })
})

export default route